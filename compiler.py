#! /usr/bin/env python3
import os
import argparse
from urllib.parse import urljoin

def read_file(filename):
    infile = open(filename)
    content = infile.read()
    infile.close()
    return [e for e in content.strip('\n').split('\n') if e.strip() != '']

def get_indent(line):
    if line == '' or line[0] != ' ':
        return 0
    return 1 + get_indent(line[1:])

def get_directive(line):
    line = line.strip()
    if line[0] != '#':
        return
    tokens = line[1:].split()
    return tokens

def compile(filename):
    source_code = read_file(filename)
    result = []
    for line in source_code:
        directive = get_directive(line)
        if directive:
            if directive[0] == 'include':
                content = compile(urljoin(filename, directive[1]))
                spaces = get_indent(line)
                for contentLine in content:
                    result.append(' ' * spaces + contentLine)
            else:
                result.append(line)
        else:
            result.append(line)
    return result

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--output-path', dest='output_path', default='dist/tongka.yaml', help='Output path')
args = parser.parse_args()

mainFile = 'src/index.yaml'
outputPath = args.output_path
if not os.path.exists(os.path.dirname(outputPath)):
    os.makedirs(os.path.dirname(outputPath))
with open(outputPath, 'w') as outfile:
    outfile.write('\n'.join(compile(mainFile)))
