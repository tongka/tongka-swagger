FROM python:3-alpine
COPY . .
RUN ./compiler.py

FROM swaggerapi/swagger-ui:latest
COPY --from=0 dist/tongka.yaml /usr/share/nginx/html/
EXPOSE 8080
ENV API_URL="tongka.yaml"
CMD ["sh", "/usr/share/nginx/run.sh"]
